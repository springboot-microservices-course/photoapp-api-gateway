FROM openjdk:11-jdk-slim
VOLUME /tmp
COPY target/PhotoAppApiGateway-0.0.1-SNAPSHOT.jar PhotoAppApiGateway.jar
ENTRYPOINT ["java", "-jar", "PhotoAppApiGateway.jar"]
